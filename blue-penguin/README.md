![screenshot](screenshot.png)

## Description
A simple theme for Pelican. Solarized pygments. Feeds support.

## Settings
```python
# all the following settings are *optional*

# HTML metadata
SITEDESCRIPTION = ''

# all defaults to True.
DISPLAY_HEADER = True
DISPLAY_FOOTER = True
DISPLAY_HOME   = True
DISPLAY_MENU   = True

# provided as examples, they make ‘clean’ urls. used by MENU_INTERNAL_PAGES.
TAGS_URL           = 'tags'
TAGS_SAVE_AS       = 'tags/index.html'
AUTHORS_URL        = 'authors'
AUTHORS_SAVE_AS    = 'authors/index.html'
CATEGORIES_URL     = 'categories'
CATEGORIES_SAVE_AS = 'categories/index.html'
ARCHIVES_URL       = 'archives'
ARCHIVES_SAVE_AS   = 'archives/index.html'

# use those if you want pelican standard pages to appear in your menu
MENU_INTERNAL_PAGES = (
    ('Tags', TAGS_URL, TAGS_SAVE_AS),
    ('Authors', AUTHORS_URL, AUTHORS_SAVE_AS),
    ('Categories', CATEGORIES_URL, CATEGORIES_SAVE_AS),
    ('Archives', ARCHIVES_URL, ARCHIVES_SAVE_AS),
)
# additional menu items
MENUITEMS = (
    ('NotABug', 'https://notabug.org/Ducker/ducker'),
)

# Languages (useful if translations are used)
LANGS = ['en', 'es']
```

## How to contribute
Contributions are very welcome. Keep in mind that this theme goal is to be
minimalistic/simple. Contributions will be accepted through NotABug Pull
Requests. If you don’t have a NotABug account you can suggest me your
changes by email (which you can find on my NotABug profile).

## Contributors
* [anhtuann](https://github.com/anhtuann)
* [aperep](https://github.com/aperep)
* [dn0](https://github.com/dn0)
* [Grimbox](https://github.com/Grimbox)
* [iranzo](https://github.com/iranzo)
* [ix5](https://github.com/ix5)
* [Jody Frankowski](http://github.com/jody-frankowski) (Blue Penguin)
* [Jorge Maldonado Ventura](https://notabug.org/jorgesumle)
* [Nevan Scott](https://github.com/nevanscott/Mockingbird) (original author)
* [SnorlaxYum](https://github.com/SnorlaxYum)
* [thetlk](https://github.com/thetlk)
* [wrl](http://github.com/wrl) (port to pelican, pelican-mockingbird)

## License
<a rel="license" href="http://creativecommons.org/publicdomain/mark/1.0/">
<img src="http://i.creativecommons.org/p/mark/1.0/88x31.png" style="border-style: none;" alt="Public Domain Mark" />
</a>
<br />
This work (<span property="dct:title">Ducker website theme</span>, by <a href="https://notabug.org/Ducker/ducker-website-theme#contributors" rel="dct:creator"><span property="dct:title">its contributors</span></a>) is free of known copyright restrictions.
