Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2019-12-20 15:30
Lang: es
Slug: objetivos
Tags: actualizar
Title: Arrancando motores

Voy a actualizar al castellano el libro [*El manual del administrador de
Debian*](https://debian-handbook.info/get/#spanish) y voy a documentar
aquí mi progreso.

La versión del libro actual es la versión 8 de Debian, pero la más
actual es la versión 10, llamada Debian Buster. Sin embargo, el libro en
inglés, que es el que se utiliza de base para las traducciones, no
está aún completamente actualizado a la versión 10; quedan por revisar
unos pocos capítulos. Una vez que esté lista la versión inglesa, se
podrá a empezar a traducir el texto.

Yo ya he puesto de mi parte en la actualización de la versión inglesa, y
estoy a la espera de que sigan mis
sugerencias en
[algunas](https://salsa.debian.org/hertzog/debian-handbook/issues/8#note_103708)
[revisiones](https://salsa.debian.org/hertzog/debian-handbook/issues/15#note_107147) de capítulos que he realizado.

Lo más sensato de actualizar ahora son las imágenes, pues las imágenes
del instalador de Debian, por ejemplo, no van a sufrir cambios. Me
pondré pronto manos a la obra.
