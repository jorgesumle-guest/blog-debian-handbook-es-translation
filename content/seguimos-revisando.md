Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2020-03-08
Lang: es
Slug: seguimos-revisando
Tags: revisión
Title: Seguimos revisando

Quedan solo por revisar tres capítulos de la versión inglesa: el [10](https://salsa.debian.org/hertzog/debian-handbook/issues/12), el [12](https://salsa.debian.org/hertzog/debian-handbook/issues/14) y el [14](https://salsa.debian.org/hertzog/debian-handbook/issues/16).

El proceso de revisión no puede realizarlo quien ha actualizado el
capítulo, que soy yo en estos casos, así que estoy a la espera de las
correcciones y sugerencias de cambios. El viernes y el sábado corregí y
mejoré las secciones del capítulo 10 sobre Let's Encrypt, OpenVPN y
easy-rsa, según [me dijo Raphaël
Hertzog](https://salsa.debian.org/hertzog/debian-handbook/issues/12#note_145907).

Lo ideal sería terminar de revisar pronto las cosillas que quedan para
poder actualizar la traducción española. Mientras tanto lo único que se
debería actualizar de la versión española son las imágenes, que
previsiblemente no sufrirán cambios, al menos las de los capítulos ya
revisados.
