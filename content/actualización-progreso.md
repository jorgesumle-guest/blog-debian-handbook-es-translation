Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2020-08-13
Lang: es
Slug: actualización-progreso
Tags: progreso
Title: Actualización del progreso

Cada vez queda menos para completar la traducción. A día de hoy está
traducido ya el 77% del libro.

<a href="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/77_por_ciento_traducido_Weblate.png">
<img src="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/77_por_ciento_traducido_Weblate.png" alt="" width="1261" height="331" srcset="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/77_por_ciento_traducido_Weblate.png 1261w, https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/77_por_ciento_traducido_Weblate-630x165.png 630w" sizes="(max-width: 1261px) 100vw, 1261px">
</a>

Respecto a contribuciones, no hay gente que haya ayudado demasiado: hay
un colaborador que solo ha traducido una cadena, por ejemplo. Yo no he
tenido mucho tiempo hasta ahora, así que he avanzado muy lentamente,
pero sin pausa.

<figure>
    <a href="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo.png">
    <img src="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo.png" alt="" width="1366" height="768" srcset="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo.png 1366w, https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo-683x384.png 683w" sizes="(max-width: 1366px) 100vw, 1366px">
    </a>
    <figcaption>Progreso según sección del libro</figcaption>
</figure>

Hay ya muchos capítulos completamente traducidos al español. Lo ideal es
ir capítulo a capítulo, y así he ido traduciendo. Por otro lado, Weblate
ha mejorado bastante en los últimos años: ahora permite hacer
comprobaciones automáticas y marca algunas cadenas como dudosas (con
color amarillo en la gráfica de las anteriores imágenes). Así se pueden
encontrar fácilmente errores como dos espacios en blanco seguidos,
etiquetas XML que no concuerdan con las que hay en el idioma original,
etc. Algunas de esas comprobaciones dan falsos positivos. Ahora mismo
estoy trabajando en resolver esas alertas que da Weblate para mejorar la
calidad de la traducción existente.
