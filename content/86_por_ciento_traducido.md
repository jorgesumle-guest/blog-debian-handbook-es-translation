Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2020-08-18 18:00
Lang: es
Slug: 86_por_ciento_traducido
Tags: Weblate, progreso, traducción
Title: 86% traducido

En este momento está traducido ya el 86% del libro.

<a href="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/86_por_ciento_traducido.png">
<img src="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/86_por_ciento_traducido.png" alt="" width="1215" height="282" srcset="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/86_por_ciento_traducido.png 1215w, https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/86_por_ciento_traducido-607x141.png 607w" sizes="(max-width: 1215px) 100vw, 1215px">
</a>

Durante la traducción he encontrado algunos errores en la versión
inglesa, así que los he corregido a medida que los he ido encontrado.

He terminado de traducir completamente el capítulo 1 y he avanzado
bastante en muchos otros.

<figure>
    <a href="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo_2.png">
    <img src="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo_2.png" alt="" width="1265" height="568" srcset="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo_2.png 1265w, https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo_2-632x284.png 632w" sizes="(max-width: 1265px) 100vw, 1265px">
    </a>
    <figcaption class="wp-caption-text">Los capítulos que quedan por traducir</figcaption>
</figure>

Las cadenas que se refieren a la salida de órdenes se deben «traducir»
de forma especial: hay que ejecutarlas en una máquina con Debian 10
configurada en español. De estas cadenas me encargaré cuando haya
terminado con las traducciones menos especiales.
