Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2020-03-14 19:00
Lang: es
Slug: actualizando-imágenes
Tags: actulizar
Title: Actualizando imágenes

[He actualizado las imágenes](https://salsa.debian.org/hertzog/debian-handbook/-/merge_requests/37) del libro.
Para la mayoría de imágenes ha bastado realizar capturas de pantalla en
máquinas virtuales; para otras ha sido necesario modificar diagramas de
[Dia](http://dia-installer.de/) y exportarlos a formato PNG. En
cualquier caso, esta actualización no está aún en la rama principal, a
la espera de que se termine la revisión de todos los capítulos en el
idioma original, el inglés.
