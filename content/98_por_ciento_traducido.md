Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2020-08-22 15:20
Lang: es
Slug: 98_por_ciento_traducido
Tags: Weblate, progreso, traducción
Title: 98% traducido

En este momento está traducido ya el 98% del libro.

<a href="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/98_por_ciento_traducido.png">
<img src="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/98_por_ciento_traducido.png" alt="" width="935" height="189" srcset="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/98_por_ciento_traducido.png 935w, https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/98_por_ciento_traducido-467x94.png 467w" sizes="(max-width: 935px) 100vw, 935px">
</a>

He encontrado algunos errores más en la versión inglesa que he
corregido.

He terminado de traducir completamente el capítulo 13 y he avanzado
bastante en muchos otros.

<figure>
    <a href="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo_3.png">
    <img src="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo_3.png" alt="" width="1265" height="572" srcset="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo_3.png 1265w, https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/progreso_según_capítulo_3-632x286.png 632w" sizes="(max-width: 1265px) 100vw, 1265px">
    </a>
    <figcaption class="wp-caption-text">Los capítulos que quedan por traducir</figcaption>
</figure>

Quedan por traducir principalmente cadenas que se refieren a la salida
de órdenes. También faltan algunas partes que tenían algún término
complicado de traducir, lo que he preferido dejar para el final.
