Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2020-08-23
Lang: es
Slug: listo
Tags: progreso, traducción, Weblate
Title: ¡Listo!

¡Ya está terminada la traducción al castellano de *El manual del
administrador de Debian*!

<a href="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/terminada-la-traducción-en-Weblate.png">
<img src="https://freakspot.net/programas/docs/manual-del-administrador-de-Debian/imagenes/2020/08/terminada-la-traducción-en-Weblate.png" alt="">
</a>

Ya se lo he comunicado a Raphäel Hertzog. Seguramente pronto se podrá
[consultar la traducción
actualizada](https://debian-handbook.info/browse/es-ES/stable/) y
[comprar el libro](https://debian-handbook.info/get/).
