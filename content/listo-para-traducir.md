Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2020-06-18
Lang: es
Slug: listo-para-traducir
Tags: traducción
Title: Listo para traducir

Ya se [publicó hace tiempo la versión
inglesa](https://debian-handbook.info/2020/debian-handbook-for-debian-10-buster-now-live/)
y cualquiera puede [traducir en Weblate](https://hosted.weblate.org/projects/debian-handbook/). Ya hay bastante texto traducido,
de la versión anterior del libro, pero aún queda mucho trabajo por
hacer.

En la lista de correo ya he hablando con alguien que está traduciendo al
español. He empezado ya a traducir y a coordinarme con otros
traductores.
