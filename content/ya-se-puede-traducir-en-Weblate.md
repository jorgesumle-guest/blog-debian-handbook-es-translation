Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2020-05-18
Lang: es
Slug: casi-listo-ya-se-puede-traducir-en-weblate
Tags: Weblate
Title: Casi listo. Ya se puede traducir en Weblate

Tras unos meses de inactividad, volvemos a la carga. Ya se puede
[traducir la versión de Debian Buster en
Weblate](https://hosted.weblate.org/projects/debian-handbook/) y
esperamos que pronto, si no encontramos problemas graves, se publique la
versión inglesa.

He aprovechado y he traducido un poquito, pero más que nada he estado
observando el estilo de la traducción, que trataré de mantener. Se usa
el usted para dirigirse al lector, y algunas traducciones no son muy
buenas. Cuando tenga más tiempo espero poder traducir lo que queda y
mejorar lo que ya hay traducido.
