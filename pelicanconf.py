#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Jorge Maldonado Ventura'
SITENAME = 'Actualización de El manual del administrador de Debian'
SITEURL = ''
THEME = 'blue'
STATIC_PATHS = ['imagenes']

PATH = 'content'

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = 'es'

THEME = 'blue-penguin/'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('El manual del administrador de Debian', 'https://debian-handbook.info/get/#spanish'),
         ('Debian', 'http://debian.org/'),
         ('Código fuente', 'https://salsa.debian.org/hertzog/debian-handbook'),)

DEFAULT_PAGINATION = 10

LOCALE = ('es_ES.UTF-8')

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
